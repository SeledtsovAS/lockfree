#ifndef STACKINPOP_H
#define STACKINPOP_H

#include <memory>
#include <atomic>
#include <iostream>

template <typename T>
class StackInPop {
private:
    struct Node {
        std::shared_ptr<T> m_data;
        Node* m_next;

        Node(const T& value):
            m_data(std::make_shared<T>(value)),
            m_next(nullptr)
        { }
    };

    std::atomic<Node*> m_head;
    std::atomic<Node*> m_to_be_deleted;
    std::atomic<int> m_count_in_pop;

private:
    void delete_nodes(Node* nodes)
    {
        Node* current = nodes;
        while (current) {
            nodes = current->m_next;
            delete current;
            current = nodes;
        }
    }

    void chain_pending_nodes(Node* first, Node* last)
    {
        last->m_next = m_to_be_deleted.load();
        while (!m_to_be_deleted.compare_exchange_weak(last->m_next, first));
    }

    void chain_pending_node(Node* node)
    {
        chain_pending_nodes(node, node);
    }

    void chain_pending_nodes(Node* first)
    {
        Node* current = first;
        while (Node* next = current->m_next) {
            current = next;
        }
        chain_pending_nodes(first, current);
    }

    void try_reclaim(Node* old_head)
    {
        if (m_count_in_pop == 1) {
            Node* nodes_to_delete = m_to_be_deleted.exchange(nullptr);
            if (!--m_count_in_pop)
                delete_nodes(nodes_to_delete);
            else if (nodes_to_delete)
                chain_pending_nodes(nodes_to_delete);

            delete old_head;
        }
        else {
            chain_pending_node(old_head);
            --m_count_in_pop;
        }
    }

public:
    StackInPop():
        m_count_in_pop { 0 },
        m_to_be_deleted { nullptr },
        m_head { nullptr }
    { }

    void push(const T& value)
    {
        Node* new_head = new Node { value };
        new_head->m_next = m_head.load();
        while (!m_head.compare_exchange_weak(new_head->m_next, new_head));
    }

    std::shared_ptr<T> pop()
    {
        ++m_count_in_pop;

        Node* old_head = m_head.load();
        while (old_head && !m_head.compare_exchange_weak(old_head, old_head->m_next));

        std::shared_ptr<T> res;
        if (old_head)
            res.swap(old_head->m_data);

        try_reclaim(old_head);

        return res;
    }
};


#endif // STACKINPOP_H
