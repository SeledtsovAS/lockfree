TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++14 -pthread -latomic
LIBS += -pthread -latomic

SOURCES += main.cpp \
    lockfreestack.cpp \
    stackinpop.cpp \
    stackhazard.cpp \
    lockfreequeue.cpp \
    sorter.cpp

HEADERS += \
    lockfreestack.h \
    stackinpop.h \
    stackhazard.h \
    lockfreequeue.h \
    sorter.h \
    parallelforeach.h

