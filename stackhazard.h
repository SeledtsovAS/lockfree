#ifndef STACKHAZARD_H
#define STACKHAZARD_H

#include <memory>
#include <atomic>
#include <thread>
#include <functional>

struct HazardPointer {
public:
    std::atomic<void*> m_pointer;
    std::atomic<std::thread::id> m_id;
};

constexpr int MaxCountHazardPointer = 120;
HazardPointer hazard_pointers[MaxCountHazardPointer];

class HPOwner {
public:
    HPOwner():
        m_ptr(nullptr)
    {
        for (int i = 0; i < MaxCountHazardPointer; ++i) {
            std::thread::id old_id;
            if (hazard_pointers[i].m_id.compare_exchange_strong(old_id, std::this_thread::get_id())) {
                m_ptr = &hazard_pointers[i];
                break;
            }
        }

        if (!m_ptr)
            throw std::runtime_error("No hazard pointers available");
    }

    ~HPOwner()
    {
        m_ptr->m_id.store(std::thread::id());
        m_ptr->m_pointer.store(nullptr);
    }

    HPOwner(const HPOwner&) = delete;
    HPOwner& operator=(const HPOwner&) = delete;

    std::atomic<void*>& get_pointer() const
    {
        return m_ptr->m_pointer;
    }

private:
    HazardPointer* m_ptr;
};

template <typename T>
class Deleter {
public:
    void operator()(void* ptr) const
    {
        delete static_cast<T*>(ptr);
    }
};

struct DataToReclaim {
public:
    DataToReclaim* m_next;
    std::function<void(void*)> m_deleter;
    void* m_data;

    template <typename T>
    DataToReclaim(T* data):
        m_next(nullptr),
        m_deleter(Deleter<T>()),
        m_data(data)
    { }
};

bool outstanding_hazard_pointers_for(void* pointer)
{
    for (int i = 0; i < MaxCountHazardPointer; ++i) {
        if (hazard_pointers[i].m_pointer.load() == pointer)
            return true;
    }
    return false;
}

class ListDataReclaim {
public:
    template <typename T>
    void reclaim_later(T* data)
    {
        add_to_reclaim_list(new DataToReclaim { data });
    }

    void delete_nodes_with_no_hazards()
    {
        DataToReclaim* old_head = m_head.exchange(nullptr);
        while (old_head) {
            DataToReclaim* const next = old_head->m_next;
            if (!outstanding_hazard_pointers_for(old_head))
                delete old_head;
            else
                add_to_reclaim_list(old_head);
            old_head = next;
        }
    }

private:
    void add_to_reclaim_list(DataToReclaim* node)
    {
        node->m_next = m_head.load();
        while (!m_head.compare_exchange_weak(node->m_next, node));
    }

private:
    std::atomic<DataToReclaim*> m_head;
};

std::atomic<void*>& get_hazard_pointer_for_this_thread()
{
    static thread_local HPOwner owner;
    return owner.get_pointer();
}

template <typename T>
class StackHazard {
private:
    struct Node {
    public:
        Node* m_next;
        std::shared_ptr<T> m_data;

        Node(const T& value):
            m_next(nullptr),
            m_data(std::make_shared<T>(value))
        { }
    };

    std::atomic<Node*> m_head;
    ListDataReclaim m_list;

public:
    StackHazard():
        m_head { },
        m_list { }
    { }

    StackHazard(const StackHazard&) = delete;
    StackHazard& operator=(const StackHazard&) = delete;

    void push(const T& value)
    {
        Node* new_head = new Node { value };
        new_head->m_next = m_head;
        while (!m_head.compare_exchange_weak(new_head->m_next, new_head));
    }

    std::shared_ptr<T> pop()
    {
        std::atomic<void*>& hp = get_hazard_pointer_for_this_thread();
        Node* old_head = m_head.load();
        do {
            Node* tmp = nullptr;
            do {
                tmp = old_head;
                hp.store(old_head);
                old_head = m_head.load();
            }
            while (tmp != old_head);
        }
        while (old_head && !m_head.compare_exchange_strong(old_head, old_head->m_next));
        hp.store(nullptr);

        std::shared_ptr<T> res;
        if (old_head) {
            res.swap(old_head->m_data);
            if (outstanding_hazard_pointers_for(old_head))
                m_list.reclaim_later(old_head);
            else
                delete old_head;
            m_list.delete_nodes_with_no_hazards();
        }
        return res;
    }
};

#endif // STACKHAZARD_H
