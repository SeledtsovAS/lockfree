#ifndef LOCK_FREE_STACK_H_
#define LOCK_FREE_STACK_H_

#include <atomic>
#include <memory>

template <typename T>
class LockFreeStack {
private:
    struct Node;

    struct CountedNodePtr {
        int m_external_counter;
        Node* m_ptr;
    };

    struct Node {
        std::shared_ptr<T> m_data;
        std::atomic<int> m_internal_counter;
        CountedNodePtr m_next;

        template <typename... Args>
        Node(Args&& ...args):
            m_data(std::make_shared<T>(std::forward<Args...>(args...))),
            m_internal_counter(0)
        { }
    };

    std::atomic<CountedNodePtr> m_head;

private:
    void increase_head_count(CountedNodePtr& old_head);

public:
    LockFreeStack();
    ~LockFreeStack();

    void push(const T& new_value);
    std::shared_ptr<T> pop();
};

template <typename T>
LockFreeStack<T>::LockFreeStack():
    m_head { } { }

template <typename T>
LockFreeStack<T>::~LockFreeStack()
{
    while (pop());
}

template <typename T>
void LockFreeStack<T>::push(const T &new_value)
{
    CountedNodePtr new_head;
    new_head.m_external_counter = 1;
    new_head.m_ptr = new Node { new_value };

    new_head.m_ptr->m_next = m_head.load();
    while (!m_head.compare_exchange_weak(new_head.m_ptr->m_next, new_head,
                        std::memory_order_release, std::memory_order_relaxed));
}

template <typename T>
std::shared_ptr<T> LockFreeStack<T>::pop()
{
    CountedNodePtr old_head = m_head.load(std::memory_order_release);
    while (true) {
        increase_head_count(old_head);
        Node* const ptr = old_head.m_ptr;
        if (!ptr)
            return std::shared_ptr<T>();
        if (m_head.compare_exchange_strong(old_head, ptr->m_next,
                 std::memory_order_relaxed, std::memory_order_relaxed)) {
            std::shared_ptr<T> res;
            res.swap(ptr->m_data);

            int const increase_count = old_head.m_external_counter - 2;
            if (ptr->m_internal_counter.fetch_add(increase_count, std::memory_order_release) == -increase_count)
                delete ptr;

            return res;
        }
        else if (ptr->m_internal_counter.fetch_sub(1) == 1) {
            std::atomic_thread_fence(std::memory_order_acquire);
            delete ptr;
        }
    }
}

template <typename T>
void LockFreeStack<T>::increase_head_count(CountedNodePtr &old_head)
{
    CountedNodePtr new_head;
    do {
        new_head = old_head;
        ++new_head.m_external_counter;
    }
    while (!m_head.compare_exchange_strong(old_head, new_head,
                 std::memory_order_acquire, std::memory_order_relaxed));
    old_head.m_external_counter = new_head.m_external_counter;
}

#endif // LOCK_FREE_STACK_H_
