#ifndef SORTER_H
#define SORTER_H

#include <list>
#include <future>
#include <vector>
#include <thread>
#include <atomic>
#include <memory>
#include <algorithm>

#include "lockfreestack.h"

template <typename T>
struct Sorter {
    struct ChunkToSort {
        std::list<T> m_data;
        std::promise<T> m_promise;
    };

    LockFreeStack<ChunkToSort> m_chunks;
    std::atomic<bool> m_endOfData;
    std::vector<std::thread> m_threads;
    unsigned const MaxThreadsCount;

    Sorter():
        MaxThreadsCount { std::thread::hardware_concurrency() - 1},
        m_endOfData { false }
    { }

    ~Sorter()
    {
        m_endOfData = false;
        for (auto i = 0; i < MaxThreadsCount; ++i)
            m_threads[i].join();
    }

    void trySortChunk()
    {
        std::shared_ptr<ChunkToSort> chunk = m_chunks.pop();
        if (chunk)
            sortChunk(chunk);
    }

    std::list<T> doSort(std::list<T>& chunkData)
    {
        if (chunkData.empty())
            return chunkData;

        std::list<T> result;
        result.splice(result.begin(), chunkData, chunkData.begin());
        T const& partitionValue = result.front();

        auto dividePoint = std::partition(chunkData.begin(), chunkData.end(),
            [&](const T& value) {
                return value < partitionValue; });

        ChunkToSort newLowerChunk;
        newLowerChunk.m_data.splice(newLowerChunk.m_data.end(), chunkData,
            chunkData.begin(), dividePoint);

        std::future<std::list<T>> newLower = newLowerChunk.m_promise.get_future();
        m_chunks.push(std::move(newLowerChunk));

        if (m_threads.size() < MaxThreadsCount)
            m_threads.push_back(std::thread(&Sorter<T>::sortThread, this));

        std::list<T> newHigher { doSort(chunkData) };

        result.splice(result.end(), newHigher);
        while (newLower.wait_for(std::chrono::seconds(0)) != std::future_status::ready)
            trySortChunk();
        result.splice(result.begin(), newLower.get());

        return result;
    }

    void sortChunk(std::shared_ptr<ChunkToSort> const& chunk)
    {
        chunk->m_promise.set_value(doSort(chunk->m_data));
    }

    void sortThread()
    {
        while (!m_endOfData) {
            trySortChunk();
            std::this_thread::yield();
        }
    }
};

#endif // SORTER_H
