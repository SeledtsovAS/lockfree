#ifndef LOCKFREEQUEUE_H
#define LOCKFREEQUEUE_H

#include <memory>
#include <atomic>

template <typename T>
class LockFreeQueue {
private:
    struct Node;

    struct CountedNodePtr {
        int m_external_counter;
        Node* m_ptr;
    };

    struct NodeCounter {
        unsigned m_internal_counter: 30;
        unsigned m_external_counters: 2;
    };

    struct Node {
        std::atomic<T*> m_data;
        std::atomic<NodeCounter> m_count;
        CountedNodePtr m_next;

        Node()
        {
            NodeCounter new_node;
            new_node.m_external_counters = 2;
            new_node.m_internal_counter = 0;
            m_count.store(new_node);

            m_next.m_ptr = nullptr;
            m_next.m_external_counter = 0;
            m_data = nullptr;
        }

        void release_ref();
    };

    std::atomic<CountedNodePtr> m_head;
    std::atomic<CountedNodePtr> m_tail;

private:
    static void increase_external_count(
            std::atomic<CountedNodePtr>& counter,
            CountedNodePtr& old_counter);
    static void free_external_counter(CountedNodePtr& old_node_ptr);

public:
    LockFreeQueue();
    ~LockFreeQueue();

    void push(const T& value);
    std::unique_ptr<T> pop();
};

template <typename T>
LockFreeQueue<T>::LockFreeQueue():
    m_head { CountedNodePtr { 1, new Node { } } },
    m_tail { m_head.load() }
{ }

template <typename T>
LockFreeQueue<T>::~LockFreeQueue()
{
    while (pop());
    delete m_head.load().m_ptr;
}

template <typename T>
void LockFreeQueue<T>::push(const T &value)
{
    std::unique_ptr<T> new_data { new T { value } };

    CountedNodePtr new_next;
    new_next.m_ptr = new Node { };
    new_next.m_external_counter = 1;

    CountedNodePtr old_tail = m_tail.load();
    while (true) {
        increase_external_count(m_tail, old_tail);
        T* old_data = nullptr;
        if (old_tail.m_ptr->m_data.compare_exchange_strong(old_data, new_data.get())) {
            old_tail.m_ptr->m_next = new_next;
            old_tail = m_tail.exchange(new_next);
            free_external_counter(old_tail);

            new_data.release();
            break;
        }
        old_tail.m_ptr->release_ref();
    }
}

template <typename T>
std::unique_ptr<T> LockFreeQueue<T>::pop()
{
    CountedNodePtr old_head = m_head.load(std::memory_order_relaxed);
    while (true) {
        increase_external_count(m_head, old_head);
        Node* const ptr = old_head.m_ptr;
        if (ptr == m_tail.load().m_ptr) {
            ptr->release_ref();
            return std::unique_ptr<T>();
        }

        if (m_head.compare_exchange_strong(old_head, ptr->m_next)) {
            T* const res = ptr->m_data.exchange(nullptr);
            free_external_counter(old_head);
            return std::unique_ptr<T>(res);
        }
        ptr->release_ref();
    }
}

template <typename T>
void LockFreeQueue<T>::Node::release_ref()
{
    NodeCounter old_counter =
        m_count.load(std::memory_order_relaxed);
    NodeCounter new_counter;

    do {
        new_counter = old_counter;
        --new_counter.m_internal_counter;
    }
    while (!m_count.compare_exchange_strong(old_counter, new_counter,
                  std::memory_order_acquire, std::memory_order_relaxed));
    if (!new_counter.m_external_counters && !new_counter.m_internal_counter)
        delete this;
}

template <typename T>
void LockFreeQueue<T>::increase_external_count(
        std::atomic<CountedNodePtr> &counter,
        CountedNodePtr &old_counter)
{
    CountedNodePtr new_counter;
    do {
        new_counter = old_counter;
        ++new_counter.m_external_counter;
    }
    while (!counter.compare_exchange_strong(old_counter, new_counter,
                std::memory_order_acquire, std::memory_order_relaxed));

    old_counter.m_external_counter = new_counter.m_external_counter;
}

template <typename T>
void LockFreeQueue<T>::free_external_counter(CountedNodePtr &old_node_ptr)
{
    Node* const ptr = old_node_ptr.m_ptr;
    int const increase_count = old_node_ptr.m_external_counter - 2;
    NodeCounter old_counter = ptr->m_count.load(std::memory_order_relaxed);

    NodeCounter new_counter;
    do {
        new_counter = old_counter;
        new_counter.m_internal_counter += increase_count;
        --new_counter.m_external_counters;
    }
    while (!ptr->m_count.compare_exchange_strong(old_counter, new_counter,
                std::memory_order_acquire, std::memory_order_relaxed));

    if (!new_counter.m_external_counters && !new_counter.m_internal_counter)
        delete ptr;
}

#endif // LOCKFREEQUEUE_H
