#include <iostream>
#include <atomic>

#include "lockfreestack.h"
#include "stackinpop.h"
#include "stackhazard.h"
#include "lockfreequeue.h"

int main(int argc, char** argv) try {
    StackInPop<int> queue;
    queue.push(1);
    queue.push(2);

    while (auto const ptr = queue.pop())
       std::cout << *ptr << std::endl;

    return 0;
}
catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return -1;
}
