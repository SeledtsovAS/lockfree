#ifndef PARALLELFOREACH_H
#define PARALLELFOREACH_H

#include <algorithm>
#include <thread>
#include <chrono>
#include <future>

template <typename Iterator, typename Functor>
void parallel_for_each(Iterator begin, Iterator end, Functor f)
{
    unsigned const length = std::distance(begin, end);
    unsigned const min_per_thread = 25;

    if (length < 2 * min_per_thread)
        std::for_each(begin, end, f);
    else {
        Iterator const mid_point = begin + length / 2;
        std::future<void> first_half =
            std::async(&parallel_for_each<Iterator, Functor>, begin, mid_point, f);
        parallel_for_each(mid_point, end, f);
        first_half.get();
    }
}

#endif // PARALLELFOREACH_H
